### Ингредиенты

* Рис для ризотто - 300 г (100 г на человека)
* Луковица - 1 шт
* Сельдерей - 1 шт
* Бульон (или хотя бы бульонный кубик) - 200 мл
* Сушеные грибы - 10 шт (лисички)
* Грибы - 500 г (лучше грибное ассорти)
* Тимьян - 3 веточки
* Розмарин - 1 веточка
* Вино белое - 1 стакан
* Петрушка - 3 веточки
* Лимон - 1 шт

* Морская соль крупная - 1 мельница

* Оливковое масло - 1 бутылка
* Сливочное масло - 1 пачка
* Пармезан - 1 шт

### Инструменты 

* Казан (или глубокая керамическая кастрюля)
* Ложка деревянная

### Подготовка

1. Замочить в гор. воде суш. грибы на 5 мин
2. Нарезать мелко лук, сельдерей, суш. грибы и листья розмарина
3. Нарезать грибы средне
4. Отделить листья тимьяна от веток
5. Нарезать ботву петрушки средне-мелко

### Инструкция

1. Поставить казан на огонь, полить ол. маслом и выложить немного слив. масла
2. Добавить лук, сельдерей, суш. грибы, розмарин и дать им пару минут
3. Выложить аккуратно весь рис
4. Залить половину стакана белого вина и дать ему 5 минут повыпариваться, перемешать
5. Добавить половину стакана бульона (или бульонный кубик с кипящей водой)
6. После того как лишняя влага исчезла выложить грибы и аккуратно перемешать, дать пару минут
7. Добавить листья тимьяна и воду из под суш. грибов, бульон, посолить, аккуратно перемешать
8. После 10 минут добавить несколько кусочков сливочного масла и потереть пармезан не жалея
9. Добавить бульона или воды, аккуратно перемешать и дать пару минут
10. Выдавить сверху треть лимона и закинуть петрушку
11. Перемешать все очень аккуратно, выключить огонь и дать отстояться минут 10
12. При подаче полить оливковым маслом и потереть пармезан сверху
 
### Результат

![Результат](/media/risotto-mushrooms.jpg)
